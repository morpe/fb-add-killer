// ==UserScript==
// @name         fb-add-killer
// @namespace    http://www.twatac.net/
// @version      0.3.1
// @description  try to take over the world!
// @author       morpe
// @match        https://www.facebook.com/*
// @require      https://code.jquery.com/jquery-3.1.1.min.js
// @grant       GM_getResourceText
// @run-at       document-end
// @resource help1 https://bitbucket.org/morpe/fb-add-killer/raw/master/help.html
// @resource theme https://bitbucket.org/morpe/fb-add-killer/raw/master/theme.css
// @updateURL    https://bitbucket.org/morpe/fb-add-killer/raw/master/fb-add-killer.js
// @downloadURL  https://bitbucket.org/morpe/fb-add-killer/raw/master/fb-add-killer.js

// ==/UserScript==

function fbak(){
    var help = GM_getResourceText("help1");
    var theme = GM_getResourceText("theme");

    jQuery("head").append("<style class='fbak-theme'>"+theme+"</style>");

    var fbbox = ".userContentWrapper";
    var tofind = "Post consigliato";
    var color = "#0f0";

    /* post consigliati */
    var items = jQuery(".userContentWrapper:contains('"+tofind+"')");
    jQuery(items).each(function(a,b){
        var id = "fbdk"+a;
        jQuery(b).addClass(id);
        jQuery(b).addClass("fbdks");
        jQuery(b).addClass("post-c-min");
        jQuery(b).height(32);
        jQuery(b).on("click",function(){
            var h = jQuery(this).data().h;
            jQuery(this).height(h);
        });

        var h = [];
        jQuery(b).children().each(function(aa,bb){
            h.push( jQuery(bb).height() );
            jQuery(b).data("childH"+h);
            jQuery(b).attr("data-child-h", JSON.stringify( h ));
        });

        jQuery(b).on("click",function(){
            var sum = 0;
            jQuery.each( jQuery(this).data().childH,function(aaa,bbb){
                sum += bbb;
            } );

            jQuery(this).height(sum);

        });

    });
    console.log("fbadk:",items.length);

    /* side bar */
    /* jQuery(window).on("scroll",function(){

        jQuery(".ego_column:contains('Crea un')").css({
            'border': '2px solid rgb(255, 0, 0)',
            'height': '34px',
            'overflow': 'hidden',
            'padding': '4px',
            'cursor':'pointer',
            'background': '#ff0'

        }).on("click",function(){

            $(this).css({
                'border': '2px solid rgb(255, 0, 0)',
                'height': 'inherit',
                'overflow': 'hidden',
                'padding': '4px',
                'background': '#ff0'
            });

            console.log($(this));

        });
    }); */
}

//jQuery.noConflict();
jQuery(document).ready(function(){

    var cc = 0;

    $(document).bind('DOMNodeInserted', function(a,b){
        cc++;
        if(cc == 25){
            //console.log("!!!",a,b);
            cc = 0;
            fbak();

        }
    });

});


